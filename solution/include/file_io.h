#include <stdbool.h>
#include <stdio.h>

bool openf(FILE** file, const char* name, const char* mode);

bool closef(FILE** file);
