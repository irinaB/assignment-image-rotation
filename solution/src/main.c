#include "../include/rotate_image.h"
#include "../include/bmp_io.h"
#include "../include/file_io.h"

#include <assert.h>
#include <malloc.h>

static const char* read_status_messages[] = {
	[READ_OK] = "Data was successfully read\n",
	[READ_INVALID_SIGNATURE] = "Invalid signature!\n",
	[READ_INVALID_BITS] = "Invalid bits!\n",
	[READ_INVALID_HEADER] = "Invalid header!\n",
	[READ_PIXELS_ERROR] = "Can't read pixels!\n"

};

static const char* write_status_messages[] = {
	[WRITE_OK] = "Data was written successfully\n",
	[WRITE_ERROR] = "Unable to write output file!!!\n"
};

static const char* ARGS_ERROR =  "You have not entered input file and output file!!!\n";
static const char* OPEN_ERROR = "Recieved open error!\n";

static void print_msg_to_stderr(const char* msg) {
	fprintf(stderr, "err: %s", msg);
}

static void print_msg_to_stdout(const char* msg) {
	printf("%s", msg);
}

static void print_msg(bool error, const char* msg){

	if (error) {
		print_msg_to_stderr(msg);
	} else {
		print_msg_to_stdout(msg);
	}
}


int main(int args, char** argv) {
	if (args != 3) 
	{
		print_msg_to_stderr(ARGS_ERROR);
		return 1;
	}
	
	FILE* in = NULL;
	FILE* out = NULL;

	const char* file_name_input = argv[1];
	const char* file_name_output = argv[2];

	if (openf(&in, file_name_input, "rb")==false || openf(&out, file_name_output, "wb")==false) {
		print_msg_to_stderr(OPEN_ERROR);
		return 1;
	}

	struct image image = { 0 };
	enum read_status rs = from_bmp( in, &image);
	print_msg(rs != READ_OK, read_status_messages[rs]);

	if (rs != READ_OK) {
		return 1;
	}

	struct image rotated_image = rotate(&image);
	enum write_status ws = to_bmp( out, &rotated_image);
	print_msg(ws != WRITE_OK, write_status_messages[rs]);

	if (ws != WRITE_OK) {
		return 1;
	}
	
	closef(&in);
	closef(&out);
	free(image.pixels);
	free(rotated_image.pixels);
	return 0;
}
