#include "../include/bmp_header.h"
#include "../include/bmp_io.h"

#include <assert.h>
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40

enum read_status bmp_header_read(FILE* in, struct bmp_header* header) {
	fseek(in, 0, SEEK_END);
	const size_t size = ftell(in);
	if (size >= sizeof(struct bmp_header)) {
		rewind(in);
		fread(header, sizeof(struct bmp_header), 1, in);
		return READ_OK;
	}
	else
	{
		return READ_INVALID_HEADER;
	}
}

enum read_status read_pixels(struct image* img, FILE* in, uint8_t padding) {
	size_t width = img->width;
	size_t height = img->height;

	struct pixel* pixels = malloc(width * height * sizeof(struct pixel));
	struct pixel* pixel_position = pixels;

	for (size_t i = 0; i < height; i++) {
		size_t count = fread(pixel_position, sizeof(struct pixel), width, in);
		if (count != width) {
			free(pixels);
			return READ_PIXELS_ERROR;
		}

		int fseek_success = fseek(in, padding, SEEK_CUR);
		if (!(fseek_success == 0) ) {
			free(pixels);
			return READ_PIXELS_ERROR;
		}
		pixel_position = pixel_position + width; 
	}
	img->pixels = pixels;
	return READ_OK;
}



uint8_t get_padding(uint32_t width) {
	return width % 4;
}



enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = { 0 };
	if (bmp_header_read(in, &header) == READ_INVALID_HEADER) {
		return READ_INVALID_SIGNATURE;
	}
	img->width = header.biWidth;
	img->height = header.biHeight;
	return read_pixels(img, in, get_padding(header.biWidth));
}



struct bmp_header create_header(const struct image* img) {
	const uint32_t img_size = (sizeof(struct pixel) * img->width + get_padding(img->width)) * img->height;
	struct bmp_header header = {
		.bfType = BMP_TYPE,
		.bfileSize = sizeof(struct bmp_header) + img_size,
		.bfReserved = 0,
		.bOffBits = sizeof(struct bmp_header),
		.biSize = BMP_HEADER_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BMP_PLANES,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = img_size,
		.biXPelsPerMeter = 0,
		.biYPelsPerMeter = 0,
		.biClrUsed = 0,
		.biClrImportant = 0,

	};
	return header;
}



enum write_status write_image(struct image const* img, FILE* out, uint8_t padding) {
	const size_t width = img->width;
	const size_t height = img->height;
	uint64_t zero = 0;
	uint64_t cnt = 0;
	for (size_t i = 0; i < height; ++i) {
		cnt += fwrite(img->pixels + i * width, sizeof(struct pixel), width, out);
		fwrite(&zero, 1, padding, out);
	}
	if (cnt == height * width) {
		return WRITE_OK;
	}
	return WRITE_ERROR;
}




enum write_status to_bmp(FILE* out, struct image* img) {
	struct bmp_header header = create_header(img);
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
		return WRITE_ERROR;
	}
	const uint8_t padding = get_padding(header.biWidth);
	return write_image(img, out, padding);
}
