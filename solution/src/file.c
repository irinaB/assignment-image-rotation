#include "../include/file_io.h"

bool openf(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    return *file != NULL;
}

bool closef(FILE** file) {
    return !fclose(*file);
}
