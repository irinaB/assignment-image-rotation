#include "../include/rotate_image.h"

#include <malloc.h>

static size_t calculate_cur_pixel(size_t cur_x, size_t cur_y, size_t img_width){
    return cur_x*img_width + cur_y;
}


static size_t calculate_new_pixel(size_t cur_x, size_t cur_y, size_t source_width, size_t source_height){
    return (source_height - 1 -cur_y)*source_width + cur_x;
}



struct image rotate(struct image const* source) {
    struct image result = { 0 };
    result.pixels = malloc(sizeof(struct pixel) * source->width * source->height);
    result.height = source->width;
    result.width = source->height;

    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            //result.pixels[i * result.width + j] = (source->pixels[(source->height - 1 - j) * source->width + i]);
            //result.pixels[calculate_cur_pixel(i, j, result.width)] = source->pixels[calculate_new_pixel(i, j, source->width, source->height)];
            result.pixels[calculate_cur_pixel(i, j, result.width)] = (source->pixels[calculate_new_pixel(i, j, source->width, source->height)]);
        }
    }

    return result;
}
